#pragma once
namespace generic{
	template <typename Iterator, typename Object>
	Iterator myfind(Iterator start, Iterator end, const Object & x) {
		for (; start != end; ++start) {
			if (*start == x)
				return start;
		}
		return end; //or start because start==end
	}
};