// MyList.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MyList.h"
#include "generic_container_template_funcs.h"
using namespace mylist;

int main()
{
	MyList<int> l5;
	MyList<int> l6;
	l5.push_back(8);
	l5.push_back(8);
	l5.push_back(19);
	l5.push_back(19);
	l5.push_back(19);
	l5.push_back(23);
	l5.push_back(48);
	l5.push_back(48);
	l5.push_back(48);
	l5.push_back(95);
	l5.push_back(108);
	l5.push_back(109);
	l5.push_back(110);

	l6.push_back(1);
	l6.push_back(1);
	l6.push_back(1);
	l6.push_back(2);
	l6.push_back(3);
	l6.push_back(4);
	l6.push_back(5);
	l6.push_back(5);
	l6.push_back(6);
	l6.push_back(7);
	l6.push_back(8);
	l6.push_back(9);
	l6.push_back(13);
	l6.push_back(19);
	l6.push_back(24);
	l6.push_back(95);
	l6.push_back(109);
	l6.push_back(109);
	l6.push_back(109);
	l6.push_back(109);

	MyList<int> l3 = l5.my_union(l6);
	//std::cout << l3.size();
	for (auto & e : l3) {
		std::cout << e << " ";
	}

	return 0;
}

