#include "stdafx.h"

#include "MyList.h"
#include "generic_container_template_funcs.h"
#include "ToStringTemplates.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace mylist;

namespace MyList_Test
{
	TEST_CLASS(UnitTest1)
	{
	private:
		MyList<int> l1;
		MyList<int> l3;
		MyList<int> l4;
		MyList<int> l5;
		MyList<int> l6;
	public:
		TEST_METHOD_INITIALIZE(init) {
			l1.push_back(1);
			l1.push_back(4);
			l1.push_back(3);
			l1.push_back(15);
			l1.push_back(6);

			l3.push_back(1);
			l3.push_back(3);
			l3.push_back(4);
			l3.push_back(4);
			l3.push_back(4);
			l3.push_back(27);
			l3.push_back(32);
			l3.push_back(152);
			l3.push_back(633);

			l4.push_back(3);
			l4.push_back(4);
			l4.push_back(4);
			l4.push_back(15);
			l4.push_back(27);

			l5.push_back(8);
			l5.push_back(8);
			l5.push_back(19);
			l5.push_back(19);
			l5.push_back(19);
			l5.push_back(23);
			l5.push_back(48);
			l5.push_back(48);
			l5.push_back(48);
			l5.push_back(95);
			l5.push_back(108);
			l5.push_back(109);
			l5.push_back(110);

			l6.push_back(1);
			l6.push_back(1);
			l6.push_back(1);
			l6.push_back(2);
			l6.push_back(3);
			l6.push_back(4);
			l6.push_back(5);
			l6.push_back(5);
			l6.push_back(6);
			l6.push_back(7);
			l6.push_back(8);
			l6.push_back(9);
			l6.push_back(13);
			l6.push_back(19);
			l6.push_back(24);
			l6.push_back(95);
			l6.push_back(109);
			l6.push_back(109);
			l6.push_back(109);
			l6.push_back(109);
		}
		TEST_METHOD(iter_swap_not_same1)
		{
			MyList<int>::iter_swap(++l1.begin(), ----l1.end());
			MyList<int> l2{};
			l2.push_back(1);
			l2.push_back(15);
			l2.push_back(3);
			l2.push_back(4);
			l2.push_back(6);
			Assert::AreEqual(l1, l2);
		}
		TEST_METHOD(iter_swap_same1)
		{
			MyList<int>::iter_swap(++++l1.begin(), ++++l1.begin());
			MyList<int> l2{};
			l2.push_back(1);
			l2.push_back(4);
			l2.push_back(3);
			l2.push_back(15);
			l2.push_back(6);
			Assert::AreEqual(l1, l2);
		}
		TEST_METHOD(find_not_in)
		{
			using namespace generic;
			Assert::IsTrue(myfind<MyList<int>::iterator>(l1.begin(), --l1.end(), 6) == --l1.end());
		}
		TEST_METHOD(find_not_in2)
		{
			using namespace generic;
			Assert::IsTrue(myfind<MyList<int>::iterator>(l1.begin(), l1.end(), 5454) == l1.end());
		}
		TEST_METHOD(find_in)
		{
			using namespace generic;
			Assert::IsTrue(myfind<MyList<int>::iterator>(l1.begin(), --l1.end(), 3) == ++++l1.begin());
		}
		TEST_METHOD(intersection1) {
			MyList<int> local_list{};
			local_list.push_back(3);
			local_list.push_back(4);
			local_list.push_back(27);
			Assert::AreEqual(l4.intersection(l3), local_list);
		}
		TEST_METHOD(my_union1) {
			MyList<int> local_list{};
			local_list.push_back(1);
			local_list.push_back(2);
			local_list.push_back(3);
			local_list.push_back(4);
			local_list.push_back(5);
			local_list.push_back(6);
			local_list.push_back(7);
			local_list.push_back(8);
			local_list.push_back(9);
			local_list.push_back(13);
			local_list.push_back(19);
			local_list.push_back(23);
			local_list.push_back(24);
			local_list.push_back(48);
			local_list.push_back(95);
			local_list.push_back(108);
			local_list.push_back(109);
			local_list.push_back(110);

			Assert::AreEqual(l5.my_union(l6), local_list);
			Assert::AreEqual(l6.my_union(l5), local_list);


		}

	};
}