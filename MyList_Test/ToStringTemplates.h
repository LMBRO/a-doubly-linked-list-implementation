#pragma once
#include "MyList.h"
#include "CppUnitTest.h"

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<typename T>
			std::wstring ToString(const mylist::MyList<T> & ml) {
				std::wstringstream ws;
				for (T const & t : ml) {
					ws << ToString(t) << L" ";
				}
				return ws.str();
			}
		}
	}
}