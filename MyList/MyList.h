#pragma once

#include <algorithm>
#include <iostream>
#include <sstream>
#include "generic_container_template_funcs.h"
using namespace std;
namespace mylist {
	template <typename Object>
	class MyList
	{
	private:
		// The basic doubly linked list node.
		// Nested inside of MyList, can be public
		// because the Node is itself private
		struct Node
		{
			Object  data;
			Node   *prev;
			Node   *next;

			Node(const Object & d = Object{ }, Node * p = nullptr, Node * n = nullptr)
				: data{ d }, prev{ p }, next{ n } { }

			Node(Object && d, Node * p = nullptr, Node * n = nullptr)
				: data{ std::move(d) }, prev{ p }, next{ n } { }
		};

	public:
		class const_iterator
		{
		public:

			// Public constructor for const_iterator.
			const_iterator() : current{ nullptr }
			{ }

			// Return the object stored at the current position.
			// For const_iterator, this is an accessor with a
			// const reference return type.
			const Object & operator* () const
			{
				return retrieve();
			}

			const_iterator & operator++ ()
			{
				current = current->next;
				return *this;
			}

			const_iterator operator++ (int)
			{
				const_iterator old = *this;
				++(*this);
				return old;
			}

			const_iterator & operator-- ()
			{
				current = current->prev;
				return *this;
			}

			const_iterator operator-- (int)
			{
				const_iterator old = *this;
				--(*this);
				return old;
			}

			bool operator== (const const_iterator & rhs) const
			{
				return current == rhs.current;
			}

			bool operator!= (const const_iterator & rhs) const
			{
				return !(*this == rhs);
			}

		protected:
			Node *current;

			// Protected helper in const_iterator that returns the object
			// stored at the current position. Can be called by all
			// three versions of operator* without any type conversions.
			Object & retrieve() const
			{
				return current->data;
			}

			// Protected constructor for const_iterator.
			// Expects a pointer that represents the current position.
			const_iterator(Node *p) : current{ p }
			{ }

			friend class MyList<Object>;
		};

		class iterator : public const_iterator
		{
		public:
			// Public constructor for iterator.
			// Calls the base-class constructor.
			// Must be provided because the private constructor
			// is written; otherwise zero-parameter constructor
			// would be disabled.
			iterator()
			{ }

			Object & operator* ()
			{
				return const_iterator::retrieve();
			}

			// Return the object stored at the current position.
			// For iterator, there is an accessor with a
			// const reference return type and a mutator with
			// a reference return type. The accessor is shown first.
			const Object & operator* () const
			{
				return const_iterator::operator*();
			}

			iterator & operator++ ()
			{
				this->current = this->current->next;
				return *this;
			}

			iterator operator++ (int)
			{
				iterator old = *this;
				++(*this);
				return old;
			}

			iterator & operator-- ()
			{
				this->current = this->current->prev;
				return *this;
			}

			iterator operator-- (int)
			{
				iterator old = *this;
				--(*this);
				return old;
			}

		protected:
			// Protected constructor for iterator.
			// Expects the current position.
			iterator(Node *p) : const_iterator{ p }
			{ }

			friend class MyList<Object>;
		};

	public:
		MyList()
		{
			init();
		}

		~MyList()
		{
			clear();
			delete head;
			delete tail;
		}

		MyList(const MyList & rhs)
		{
			init();
			for (auto & x : rhs)
				push_back(x);
		}

		MyList & operator= (const MyList & rhs)
		{
			MyList copy = rhs;
			std::swap(*this, copy);
			return *this;
		}


		MyList(MyList && rhs)
			: theSize{ rhs.theSize }, head{ rhs.head }, tail{ rhs.tail }
		{
			rhs.theSize = 0;
			rhs.head = nullptr;
			rhs.tail = nullptr;
		}

		MyList & operator= (MyList && rhs)
		{
			std::swap(theSize, rhs.theSize);
			std::swap(head, rhs.head);
			std::swap(tail, rhs.tail);

			return *this;
		}

		// Return iterator representing beginning of list.
		// Mutator version is first, then accessor version.
		iterator begin()
		{
			return iterator(head->next);
		}

		const_iterator begin() const
		{
			return const_iterator(head->next);
		}

		// Return iterator representing endmarker of list.
		// Mutator version is first, then accessor version.
		iterator end()
		{
			return iterator(tail);
		}

		const_iterator end() const
		{
			return const_iterator(tail);
		}

		// Return number of elements currently in the list.
		int size() const
		{
			return theSize;
		}

		// Return true if the list is empty, false otherwise.
		bool empty() const
		{
			return size() == 0;
		}

		void clear()
		{
			while (!empty())
				pop_front();
		}

		// front, back, push_front, push_back, pop_front, and pop_back
		// are the basic double-ended queue operations.
		Object & front()
		{
			return *begin();
		}

		const Object & front() const
		{
			return *begin();
		}

		Object & back()
		{
			return *--end();
		}

		const Object & back() const
		{
			return *--end();
		}

		void push_front(const Object & x)
		{
			insert(begin(), x);
		}

		void push_back(const Object & x)
		{
			insert(end(), x);
		}

		void push_front(Object && x)
		{
			insert(begin(), std::move(x));
		}

		void push_back(Object && x)
		{
			insert(end(), std::move(x));
		}

		void pop_front()
		{
			erase(begin());
		}

		void pop_back()
		{
			erase(--end());
		}

		// Insert x before itr.
		iterator insert(iterator itr, const Object & x)
		{
			Node *p = itr.current;
			++theSize;
			return iterator(p->prev = p->prev->next = new Node{ x, p->prev, p });
		}

		// Insert x before itr.
		iterator insert(iterator itr, Object && x)
		{
			Node *p = itr.current;
			++theSize;
			return iterator(p->prev = p->prev->next = new Node{ std::move(x), p->prev, p });
		}

		// Erase item at itr.
		iterator erase(iterator itr)
		{
			Node *p = itr.current;
			iterator retVal(p->next);
			p->prev->next = p->next;
			p->next->prev = p->prev;
			delete p;
			--theSize;

			return retVal;
		}

		iterator erase(iterator from, iterator to)
		{
			for (iterator itr = from; itr != to; )
				itr = erase(itr);

			return to;
		}
		bool operator==(MyList<Object> const & other) const {
			if (this->size() != other.size())
				return false;
			const_iterator it1 = this->begin();
			const_iterator it2 = other.begin();
			while (it1 != this->end() && it2 != other.end()) {
				if (*it1 != *it2)
					return false;
				++it1;
				++it2;
			}
			return true;
		}
		//Assumes that both lists are sorted 
		MyList intersection(MyList<Object> const & other) const {
			typedef  MyList<Object> L;

			L newList = L{};
			if (this->size() == 0 || other.size() == 0)
				return newList;

			const_iterator it1 = this->begin();
			const_iterator it2 = other.begin();

			bool end1{ it1 == this->end() };
			bool end2{ it2 == other.end() };

			while (!end1 && !end2) {

				if (*it1 == *it2) {
					newList.push_back(*it1);
					it1 = MyList<Object>::goToNextDiffElement(*this, it1);
					it2 = MyList<Object>::goToNextDiffElement(other, it2);
				}
				else if (*it1 < *it2) {
					it1 = MyList<Object>::goToNextDiffElement(*this, it1);
				}
				else if (*it2 < *it1) {
					it2 = MyList<Object>::goToNextDiffElement(other, it2);
				}
				end1 = it1 == this->end();
				end2 = it2 == other.end();
			}
			return newList;
		}
		//Assumes that both lists are sorted
		MyList my_union(MyList<Object> const & other) const {
			typedef MyList<Object> L;
			L newList = L{};

			const_iterator it1 = this->begin();
			const_iterator it2 = other.begin();

			bool end1{ it1 == this->end() };
			bool end2{ it2 == other.end() };

			while (!end1 && !end2) {
				if (*it1 == *it2) {
					newList.push_back(*it1);
					it1 = MyList<Object>::goToNextDiffElement(*this, it1);
					it2 = MyList<Object>::goToNextDiffElement(other, it2);
				}
				else if (*it1 < *it2) {
					newList.push_back(*it1);
					it1 = MyList<Object>::goToNextDiffElement(*this, it1);
				}
				else if (*it2 < *it1) {
					newList.push_back(*it2);
					it2 = MyList<Object>::goToNextDiffElement(other, it2);
				}
				end1 = it1 == this->end();
				end2 = it2 == other.end();
			}
			if (end1 && !end2) {
				while (!end2) {
					newList.push_back(*it2);
					it2 = MyList<Object>::goToNextDiffElement(other, it2);
					end2 = it2 == other.end();
				}
			}
			else if (!end1 && end2) {
				while (!end1) {
					newList.push_back(*it1);
					it1 = MyList<Object>::goToNextDiffElement(*this, it1);
					end1 = it1 == this->end();
				}
			}
			return newList;
		}
		/*
		throws invalid_argument if the two iterators are not ad
		*/
		static void iter_swap(iterator & it1, iterator & it2) {
			using std::swap;
			if (it1 == it2) return;
			swap(it1.current->prev->next, it2.current->prev->next);
			swap(it1.current->next->prev, it2.current->next->prev);
			swap(it1.current->next, it2.current->next);
			swap(it1.current->prev, it2.current->prev);
		}



		//using generic::myfind;
		//MyList inter_list{};
		//for (const_iterator it1 = this->begin();it1 != this->end(); it1++) {
		//	const_iterator result = myfind<MyList<Object>::const_iterator, Object>(other.begin(), other.end(), *it1);
		//	if (result != other.end() ) {
		//		inter_list.push_back(*result);
		//	}
		//}
		//return inter_list;


	//MyList intersection2(MyList<Object> const & other) const {
	//	const_iterator it1 = this->begin();
	//	const_iterator it2 = other.begin();
	//	for (; it1;) {

	//	}


	// }
	private:
		int   theSize;
		Node *head;
		Node *tail;

		void init()
		{
			theSize = 0;
			head = new Node;
			tail = new Node;
			head->next = tail;
			tail->prev = head;
		}
		static const_iterator goToNextDiffElement(MyList<Object> const & aList, const_iterator it) {
			Object const & initialValue = *it;
			const_iterator END_IT = aList.end();
			while (it != END_IT && *it == initialValue) {
				++it;
			}
			return it;
		}
	};

	template <typename Object>
	std::ostream & operator<<(std::ostream& os, mylist::MyList<Object> const & l) {
		for (Object const & obj : l) {
			os << obj << " ";
		}
		os << std::endl;
		return os;
	}
}
